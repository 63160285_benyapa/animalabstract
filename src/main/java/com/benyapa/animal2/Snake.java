/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.animal2;

/**
 *
 * @author bwstx
 */
public class Snake extends Reptile {

    private String name;

    public Snake(String name) {
        super("Snake", 0);
        this.name = name;
    }

    @Override
    public void crawl() {
        System.out.println("Snake : " + name + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake : " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake : " + name + " can't walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake : " + name + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake : " + name + " sleep");
    }

}
