/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.animal2;

/**
 *
 * @author bwstx
 */
public class TestAnimal {

    public static void main(String[] args) {
        //Human
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("----------");
        System.out.println("h1 is Animal? : " + (h1 instanceof Animal));
        System.out.println("h1 is Land Animal? : " + (h1 instanceof LandAnimal));

        System.out.println("----------");
        Animal a1 = h1;
        System.out.println("a1 is Land Animal? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile? " + (a1 instanceof Reptile));
        System.out.println("----------");

        //Cat
        Cat c1 = new Cat("Pik");
        c1.eat();
        c1.walk();
        c1.run();
        System.out.println("----------");
        System.out.println("c1 is Animal? : " + (c1 instanceof Animal));
        System.out.println("c1 is Land Animal? : " + (c1 instanceof LandAnimal));

        System.out.println("----------");
        Animal a2 = c1;
        System.out.println("a2 is Land Animal? : " + (a2 instanceof LandAnimal));
        System.out.println("a2 is Reptile? : " + (a2 instanceof Reptile));
        System.out.println("a2 is Poultry? : " + (a2 instanceof Poultry));
        System.out.println("a2 is Aquatic Animal? : " + (a2 instanceof AquaticAnimal));
        System.out.println("----------");

        //Dog
        Dog d1 = new Dog("Chabu");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("----------");
        System.out.println("d1 is Animal? : " + (d1 instanceof Animal));
        System.out.println("d1 is Land Animal? : " + (d1 instanceof LandAnimal));

        System.out.println("----------");
        Animal a3 = d1;
        System.out.println("a3 is Land Animal? : " + (a3 instanceof LandAnimal));
        System.out.println("a3 is Reptile? : " + (a3 instanceof Reptile));
        System.out.println("a3 is Poultry? : " + (a3 instanceof Poultry));
        System.out.println("a3 is Aquatic Animal? : " + (a3 instanceof AquaticAnimal));
        System.out.println("----------");

        //Crocodile
        Crocodile cr = new Crocodile("Jimmy");
        cr.eat();
        cr.walk();
        cr.crawl();
        cr.speak();
        cr.sleep();
        System.out.println("----------");
        System.out.println("cr is Animal? : " + (cr instanceof Animal));
        System.out.println("cr is Reptile? : " + (cr instanceof Reptile));
        System.out.println("----------");

        //Snake
        Snake snake = new Snake("Blue");
        snake.eat();
        snake.walk();
        snake.crawl();
        snake.speak();
        snake.sleep();
        System.out.println("----------");
        System.out.println("snake is Animal? : " + (snake instanceof Animal));
        System.out.println("snake is Reptile? : " + (snake instanceof Reptile));
        System.out.println("----------");

        //Fish
        Fish fish = new Fish("Gold");
        fish.eat();
        fish.walk();
        fish.swim();
        fish.speak();
        fish.sleep();
        System.out.println("----------");
        System.out.println("fish is Animal? : " + (fish instanceof Animal));
        System.out.println("fish is Aquatic Animal? : " + (fish instanceof AquaticAnimal));
        System.out.println("----------");

        //Crab
        Crab crab = new Crab("Som");
        crab.eat();
        crab.walk();
        crab.swim();
        crab.speak();
        crab.sleep();
        System.out.println("----------");
        System.out.println("crab is Animal? : " + (crab instanceof Animal));
        System.out.println("crab is Aquatic Animal? : " + (crab instanceof AquaticAnimal));
        System.out.println("----------");

        //Bat
        Bat bat = new Bat("Black");
        bat.eat();
        bat.walk();
        bat.fly();
        bat.speak();
        bat.sleep();
        System.out.println("----------");
        System.out.println("bat is Animal? : " + (bat instanceof Animal));
        System.out.println("bat is Poultry? : " + (bat instanceof Poultry));
        System.out.println("----------");

        //Bird
        Bird bird = new Bird("Pooh");
        bird.eat();
        bird.walk();
        bird.fly();
        bird.speak();
        bird.sleep();
        System.out.println("----------");
        System.out.println("bird is Animal? : " + (bird instanceof Animal));
        System.out.println("bird is Poultry? : " + (bird instanceof Poultry));
        System.out.println("----------");

    }

}
