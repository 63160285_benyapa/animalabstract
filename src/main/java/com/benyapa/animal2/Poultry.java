/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.animal2;

/**
 *
 * @author bwstx
 */
public abstract class Poultry extends Animal {

    public Poultry(String name, int numOfLeg) {
        super(name, numOfLeg);
    }

    public abstract void fly();

}
